A Docker container for developing using OpenJDK and Spring.
A ready Docker image can be downloaded from: https://cloud.docker.com/repository/docker/aot29/spring

# Building and starting
Build (the first time around) and run the image: `docker-compose up -d`
This will create a directory `workspace` on the host, and mount it as `/workspace`in the container. Files in this directory will persist if you restart the container.

# Executing the example project
Open a console: `docker exec -ti spring bash`

You can now run Maven, e.g. https://maven.apache.org/guides/getting-started/index.html
and Spring, e.g. https://www.mkyong.com/spring/quick-start-maven-spring-example/

```
cd springExample/
mvn exec:java -Dexec.mainClass="org.smultron.App"
```

The example project is defined by:
* pom.xml
* src/main/resources/Spring-Module.xml
* src/main/java/org/smultron/HelloWorld.java
* src/main/java/org/smultron/App.java

# Stopping the container
`docker-compose down`

# Notes:
**Creating a new Spring project** called `org.smultron.springExample`

```
mvn -B archetype:generate -DarchetypeGroupId=org.apache.maven.archetypes -DgroupId=org.smultron -DartifactId=springExample`
cd springExample/
```

**Add Spring as a dependency** to the project object mode `pom.xml`

```
   <!-- Spring framework -->
    <dependency>
      <groupId>org.springframework</groupId>
        <artifactId>spring</artifactId>
        <version>2.5.6</version>
     </dependency>
```

Reload dependencies
```
mvn clean install -U
```

**Define a Spring Bean** in Spring-Module.xml

```
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.springframework.org/schema/beans
	http://www.springframework.org/schema/beans/spring-beans-2.5.xsd">
	<bean id="helloBean" class="org.smultron.HelloWorld">
		<property name="name" value="Hi" />
	</bean>
</beans>
```

Then edit `App.java`

**Execute the App**

`mvn exec:java -Dexec.mainClass="org.smultron.App"`
