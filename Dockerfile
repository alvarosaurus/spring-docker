FROM openjdk:8-stretch

RUN apt-get update && apt-get -y install zip sed nano less

ADD resources/commons-logging-1.2-bin.tar.gz /usr/local
ADD resources/spring-framework-5.1.8.RELEASE-dist.tar.gz /opt
ADD resources/apache-maven-3.6.1-bin.tar.gz /opt

ENV PATH /opt/spring-framework-5.1.8.RELEASE/libs:/opt/apache-maven-3.6.1/bin:$PATH

